from rest_framework import pagination


class PostPagination(pagination.LimitOffsetPagination):
    default_limit = 30
    max_limit = 100
