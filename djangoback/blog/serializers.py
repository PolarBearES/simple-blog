from rest_framework import serializers

from blog.models import Post, Comment


class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['pk', 'user', 'comment']


class SimplePostSerializer(serializers.ModelSerializer):



    class Meta:
        model = Post
        fields = ['title', 'slug', 'image_header', 'description','link']

    description = serializers.SerializerMethodField(
        method_name='calculate_description')

    def calculate_description(self, post: Post):
        return post.post[:post.post.find('</p>') + 4]

    link = serializers.SerializerMethodField('get_link')
    def get_link(self, post):
        return '/post/'+post.slug


class UserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField(max_length=100)


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ['pk', 'title', 'slug', 'image_header', 'post', 'created', 'modified']


class PostSerializerUser(PostSerializer):
    user = UserSerializer()

    class Meta():
        model = Post
        fields = ['pk', 'title', 'slug', 'image_header', 'post', 'created', 'modified', 'user']
