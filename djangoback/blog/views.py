from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from pagination import PostPagination
from .models import Post, Comment
from .serializers import PostSerializer, CommentsSerializer, SimplePostSerializer, PostSerializerUser


class PostViewSet(ModelViewSet):
    @method_decorator(cache_page(60))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)
    @method_decorator(cache_page(60))
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)
    pagination_class = PostPagination
    pagination_class.max_limit = 100
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['@title', '@post','user__username', 'user__email']
    lookup_field = 'slug'
    ordering_fields = ['created']
    ordering = ['-created']
    def get_queryset(self):
        if self.action == 'retrieve':
            return Post.objects.select_related('user').all()
        return Post.objects.all()
    def get_serializer_class(self):
        if self.action == 'list':
            return SimplePostSerializer
        elif self.action == 'retrieve':
            return PostSerializerUser
        return PostSerializer


class CommentsViewSet(ModelViewSet):
    serializer_class = CommentsSerializer

    def get_serializer_context(self):
        return {'post_slug': self.kwargs['post_slug']}

    def get_queryset(self):
        return Comment.objects.filter(post__slug=self.kwargs['post_slug'])