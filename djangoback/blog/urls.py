from django.urls import path, include
from rest_framework import routers
from rest_framework_nested import routers
from blog import views

router = routers.DefaultRouter()
router.register('posts', views.PostViewSet,basename='posts')
posts_router = routers.NestedDefaultRouter(
    router, 'posts', lookup='post')
posts_router.register('comments', views.CommentsViewSet, basename='post-comments')

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(posts_router.urls)),
]