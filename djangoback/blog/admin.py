from django.contrib import admin

# Register your models here.
from django.utils.html import format_html

from blog.models import Post, Comment
from djangoback.settings import MEDIA_URL


class CommentInline(admin.TabularInline):
    model = Comment

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'title','thumbnail')
    search_fields = ('title', 'user__username', 'user__email')
    list_filter = ('created', 'modified')
    prepopulated_fields = {
        'slug': ['title']
    }
    inlines = [CommentInline]

    def get_form(self, request, obj=None, **kwargs):
        form = super(PostAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['user'].initial = request.user
        return form

    def thumbnail(self, instance):
        if instance.image_header != '':
            return format_html(f'<a href="{instance.image_header.url}"><img src="{instance.image_header.url}" style="height: 100px;" /></a>')
        return ''

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass