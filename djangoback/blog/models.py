from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    image_header = models.ImageField(null=True,blank=True, upload_to='posts/photos')
    post = RichTextUploadingField()

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return f'{self.title} by @{self.user.username}'


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.PROTECT)
    user = models.CharField(max_length=255)
    comment = models.CharField(max_length=5000)

    def __str__(self):
        return f'{self.user}: {self.comment}'
