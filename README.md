# Simple Blog



## Getting started

Simply run docker-compose up -d to run the entire project.

~~Inside the backend container run manage.py collectstatic && manage.py migrate~~

To apply migrations
`docker exec simple-blog_django_1 python manage.py migrate`

To create superuser
`docker exec -it simple-blog_django_1 python manage.py createsuperuser`

Go to http://blog.localhost:8000/ for the frontend.

Go to http://api.blog.localhost:8000/ for the backend.

